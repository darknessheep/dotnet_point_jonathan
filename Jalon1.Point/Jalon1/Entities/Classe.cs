﻿using Jalon1.modèle;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jalon1.Entities
{
    class Classe
    {
        public string ClassId { get; set; }
        public string NomEtablissement { get; set; }
        public string niveau { get;  set; }

        public List<Eleve> Eleves { get; }
    }
}
