﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jalon1.modèle
{
    public class Eleve
    {
        [Key]
        public int EleveId { get; set; }


        public string Nom { get; set; }

        [StringLength(20)]
        [Required]
        public string Prenom { get; set; }
        public DateTime DateNaissance { get; set; }

        public int ClassID { get; set; }
    }
}
