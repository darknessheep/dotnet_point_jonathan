﻿using Jalon1.modèle;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jalon1.Mapping
{
    class EleveMapping :EntityTypeConfiguration<Eleve>
    {
        public EleveMapping()
        {
            ToTable("APP_Eleve");
            HasKey(e => e.EleveId);

            Property(e => e.EleveId).IsRequired().HasDatabaseGeneratedOption(System.ComponentModel.DataAnnotations.Schema.DatabaseGeneratedOption.Identity);
            Property(e => e.Nom).HasColumnName("ELV_NAME").IsRequired().HasMaxLength(20);

            
        }
    }
}
