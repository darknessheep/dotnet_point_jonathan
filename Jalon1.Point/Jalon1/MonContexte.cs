﻿using Jalon1.Mapping;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Jalon1
{
    public class MonContexte : DbContext
    {
        public MonContexte() : base("name=LeNomDeMaChaineDeConnection")
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Configurations.Add(new EleveMapping());

            //
        }

        public DbSet<modèle.Eleve> Eleves { get; set; }
    }
}
