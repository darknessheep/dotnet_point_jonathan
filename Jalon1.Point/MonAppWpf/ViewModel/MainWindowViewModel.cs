﻿using MonAppWpf.ViewModel.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MonAppWpf.ViewModel
{
    public class MainWindowViewModel : BaseViewModel
    {

        private ListeEtudiantViewModel _listeEtudiantViewModel;

        #region Contructors

        public MainWindowViewModel()
        {

        }

        #endregion

        #region Properties
        public ListeEtudiantViewModel ListeEtudiantViewModel
        {
            get
            {
                return _listeEtudiantViewModel;
            }

            set
            {
                _listeEtudiantViewModel = value;
            }
        }
        #endregion

    }
}
